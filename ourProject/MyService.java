package com.yournamespace.yourappname;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import org.apache.cordova.example.R;

import com.red_folder.phonegap.plugin.backgroundservice.BackgroundService;

public class MyService extends BackgroundService {
	
	private final static String TAG = MyService.class.getSimpleName();
	
	private String mHelloTo = "World";
	String line = null;
	@Override
	protected JSONObject doWork() {
		JSONObject result = new JSONObject();
		File sdcard = Environment.getExternalStorageDirectory();
		File file = new File(sdcard,"todo/newtest.txt");
		StringBuilder text = new StringBuilder();
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    while ((line = br.readLine()) != null) {
		        text.append(line);
		    }
		}
		catch (IOException e) {
		}
		JSONArray array = null;
		if(text.toString().length()!=0){
		try {
			array = new JSONArray(text.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		JSONObject row = null;
		for (int i = 0; i < array.length(); i++) {
			row = array.optJSONObject(i);
			Time time = new Time();
			time.setToNow();
			String nowtime = time.year + "-" + (time.month+1) + "-" + time.monthDay + "T" + time.hour + ":" + time.minute;
			String complete = row.optString("done");
			if (nowtime.equals(row.optString("deadline")) && complete.equals("0") ){
				try {
					row.put("overdue", 1);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				FileOutputStream fOut = null;
				try {
					fOut = new FileOutputStream(file);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);

	            try {
					myOutWriter.append(array.toString());
		            myOutWriter.close();
		            fOut.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            String titel = null;
	    
	            String timeForNoti = null;
	            try {
					titel = row.getString("titel");
				} catch (JSONException e) {
					e.printStackTrace();
				}
	            try {
					timeForNoti = row.getString("deadline");
				} catch (JSONException e) {
					e.printStackTrace();
				}
	            
	      
	            Intent intent = new Intent(this, org.apache.cordova.example.example.class);
	            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

	            /**
	             * Build notification
	             */
	            Notification noti = new Notification.Builder(this)
	                .setContentTitle("you have a todo... " + titel )
	                .setContentText("Expired on: " + timeForNoti).setSmallIcon(R.drawable.icon2)
	                .setContentIntent(pIntent).build();
	            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	            /**
	             *  Hide the notification after its selected
	             */
	            noti.flags |= Notification.FLAG_AUTO_CANCEL;
	            notificationManager.notify(0, noti);
	            
				}
			}
		}
		return result;	
	}
/**
 * Default class of background service
 */
	@Override
	protected JSONObject getConfig() {
		JSONObject result = new JSONObject();		
		return result;
	}
	/**
	 * Default class of background service
	 */
	@Override
	protected void setConfig(JSONObject config) {		
	}     
	/**
	 * Default class of background service
	 */
	@Override
	protected JSONObject initialiseLatestResult() {
		return null;
	}
	/**
	 * Default class of background service
	 */
	@Override
	protected void onTimerEnabled() {
	}
	/**
	 * Default class of background service
	 */
	@Override
	protected void onTimerDisabled() {
		
	}

}
